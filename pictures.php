<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/" );
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Sandkassen</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Sandkassen" />
    <meta name="description" content="En plass for å leke seg" />
    <meta name="keywords" content="Stephan, sandbox, php, mysql, ajax, apache2" />
    <meta name="language" content="no" />
    <meta name="subject" content="En plass for å leke seg" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Stephan Kristiansen" />
    <meta name="abstract" content="En plass for å leke seg med programmering og lignende" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <link rel="stylesheet" type="text/css" href="style.css" />
  </head>
  <body> 
    <div id="wrapper"> 
      <div id="bg"> 
        <div id="header"></div>  
        <div id="page"> 
          <div id="container"> 
            <!-- banner -->  
            <div id="banner"></div>  
            <!-- end banner -->  
            <!-- horizontal navigation -->  
            <div id="nav1"> 
              <?php 
				include("menu.php"); 
			   ?>
            </div>  
            <!-- end horizontal navigation -->  
            <!--  content -->  
            <div id="content"> 
              <div id="center"> 
                <div id="welcome"> 
                  <h2 class="welcome">Bildeseksjon</h2>
                  <p>
                  Her kommer mine bilder etterhvert... 
                  </p>  
                 
                </div> 
              </div>  
              <div id="right"> 
                <div id="sidebar"> 
                  <?php 
                  	include("categories.php");
                  	include("aboutme.php");
                  	include("rightside.php");
                  ?> 
                  </div> 
                </div> 
              </div>  
              <div class="clear" style="height:40px"/> 
            </div>  
            <!-- end content --> 
          </div>  
          <!-- end container --> 
        </div>  
           <?php 
             include("bottommenu.php")
           ?>
      </div>
      </div>  
  </body>
</html>