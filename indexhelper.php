<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/" );
require_once("config.php");

function getBlogposts(){
	mysql_connect(DB_HOST, DB_USER, DB_PASSWORD)or die("cannot connect server ");
	mysql_select_db(DB_DATABASE)or die("cannot select DB");
	$tbl_name="blogpost"; // Table name
	$tbl_name2="members";
	$category=null;
	// how many rows to show per page
	$rowsPerPage = 10;
	
	// by default we show first page
	$pageNum = 1;
	
	// if $_GET['cat'] defined, use it to limit retrieval of blogposts
	if(isset($_GET['cat']))
	{
    	$category = $_GET['cat'];
	}
	
	// if $_GET['page'] defined, use it as page number
	if(isset($_GET['page']))
	{
    $pageNum = $_GET['page'];
	}
	
	// counting the offset
	$offset = ($pageNum - 1) * $rowsPerPage;
	$sql=null;
	//get blogposts
	if($category !=null){
		$sql="SELECT * FROM $tbl_name where category_id=$category ORDER BY blogpost_id DESC LIMIT $offset, $rowsPerPage";
	}
	else{
		$sql="SELECT * FROM $tbl_name ORDER BY blogpost_id DESC LIMIT $offset, $rowsPerPage";
	}
	
	$result=mysql_query($sql);
	
		if($result){
			
			
			while($rows=mysql_fetch_array($result)) {
				
				$sql2="SELECT * FROM $tbl_name2 where member_id =".$rows['writtenby_id'];
				$result2=mysql_query($sql2);
				$row = mysql_fetch_object($result2);
				$timezone = new DateTimeZone( "Europe/Oslo" ); 
				$dateRaw = new DateTime($rows['time']); 
				$dateRaw->setTimezone( $timezone );
				$dateAsString = $dateRaw->format("Y-m-d H:i:s"); 
	 			//$date= substr($rows['time'],8,2).".".substr($rows['time'],5,2).".".substr($rows['time'],2 ,2)." kl. ";
	 			$date= substr($dateAsString,8,2).".".substr($dateAsString,5,2).".".substr($dateAsString,0 ,4);	
				echo "<h3><u>".$rows['title']."</u></h3>";
                                echo "<font size=1>skrevet den ".$date."</font>";
				echo  stripslashes($rows['blogtext']);
                		//echo "<p class=\"signpost\">skrevet av ".$row->firstname." den ".$date."</p>";
                		$thisid = $rows['blogpost_id'];
                		$testsql = "SELECT * FROM comment where blogpost_id=$thisid";
                		$resultnumcom = mysql_query($testsql);
                		$num_comments = mysql_num_rows($resultnumcom);

                		echo "<p><a style=\"color:blue; \" href=\"/?site=displayPost&blogid=".$rows['blogpost_id']."\">Gi en kommentar! (".$num_comments.")</a></p>";
                echo "<p style=\"clear:both\" />";
			 }
		}
		else{
			echo "<h3>Ingen poster her ennå</h3>"; 
			echo  "Du valgte mest sannsynlig en kategori uten blogposter. Alternativet er at noe gikk galt, men det kan jo ikke være tilfelle ;)";
		}	
		
	mysql_close(); //close database
}

function displayPost($bid,$captchaError,$nameError,$messageError){
	mysql_connect(DB_HOST, DB_USER, DB_PASSWORD)or die("cannot connect server ");
	mysql_select_db(DB_DATABASE)or die("cannot select DB");
	$tbl_name="blogpost"; // Table name
	$tbl_name2="members";
	$tbl_name3="comment";
	$category=null;
	
	$sql=null;
	//get blogpost
	$sql="SELECT * FROM $tbl_name where blogpost_id=$bid";	
	$result=mysql_query($sql);
	
	$sqlComment="SELECT * FROM $tbl_name3 where blogpost_id = $bid ORDER BY comment_id DESC";
	$resultComment=mysql_query($sqlComment);
	//$resultsize = mysql_num_rows($resultComment);
	$resultsize=1;
		
		if($result){
				$rows=mysql_fetch_array($result);
				$sql2="SELECT * FROM $tbl_name2 where member_id =".$rows['writtenby_id'];
				$result2=mysql_query($sql2);
				$row = mysql_fetch_object($result2);
				$timezone = new DateTimeZone( "Europe/Oslo" ); 
				$dateRaw = new DateTime($rows['time']); 
				$dateRaw->setTimezone( $timezone );
				$dateAsString = $dateRaw->format("Y-m-d H:i:s"); 
	 			//$date= substr($rows['time'],8,2).".".substr($rows['time'],5,2).".".substr($rows['time'],2 ,2)." kl. ";
	 			$date= substr($dateAsString,8,2).".".substr($dateAsString,5,2).".".substr($dateAsString,0 ,4);	
				echo "<h3><u>".$rows['title']."</u></h3>";
                                echo "<font size=1>skrevet den ".$date."</font>";
				echo  stripslashes($rows['blogtext']);
                //echo "<p class=\"signpost\">skrevet av ".$row->firstname." den ".$date."</p>";
                echo "<p style=\"clear:both\" />";
                
                /* legge til kommentar */
                
                echo ' <table width="500" border="0" align="center" cellpadding="0" cellspacing="1">
							<tr>
								<td><h3>Legg igjen en kommentar</h3></td>
							</tr>
						  </table>
			              <p style="clear:both" />
			              
							  <table width="500" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#D8D8D8">
									<tr>
										<td>';
										echo	"<form id=\"form1\" name=\"form1\" method=\"post\" action=\"/comments/addcomment.php\">";
										echo    "<input type=hidden name=\"blogid\" value=\"$bid\"/>";		
										echo	'<table width="500" border="0" cellpadding="2" cellspacing="1" bgcolor="#E8E8E8">
												<tr>
													<td>Navn:</td>
													<td><input name="name" type="text" id="name" size="25" />
														<br/><span class="error" name="nameError" id="nameError"></span>';
														if($nameError!=null && $nameError=='true'){
															echo '<br/><span class="error" name="nameError" id="nameError">
																Du skrev ikke inn navnet ditt, gjør et nytt forsøk!
															</span>';
														}
												echo	'</td>
												</tr>								
												<tr>
													<td valign="top">Kommentar:</td>
													<td><textarea name="comment" cols="40" rows="3" id="comment"></textarea>
														<br/><span class="error" name="commentError" id="commentError"></span>';
														if($messageError!=null && $messageError=='true'){
															echo '<br/><span class="error" name="messageError" id="messageError">
																Du skrev ikke inn noen kommentar, gjør et nytt forsøk!
															</span>';
														}
												echo	'</td>
												</tr>
												<tr>
													<td>Skriv inn tekst på bildet:</td>
													<td><input type="text" name="captcha_code" size="10" maxlength="6" />';
													if($captchaError!=null && $captchaError=='true'){
														echo '<br/><span class="error" name="captchaError" id="captchaError">
																Du skrev ikke inn rett tekst, gjør et nytt forsøk!
															</span>';
													}
												echo	'</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td><img id="captcha" src="/captcha/securimage_show.php" alt="CAPTCHA Image" />
													
													</td>
												</tr>
												<tr>
													<td>&nbsp;</td>								
													<td><input type="submit" id="submit" name="Submit" value="Legg til"/> </td>
												</tr>
												
											</table>
											</form>
										</td>						
									</tr>
								</table>
							<p style="clear:both" />
							<p style="clear:both" />				
						';
                
		
			
                while($rows2=mysql_fetch_array($resultComment)) {
					echo "<table width=\"500\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"1\" bgcolor=\"#CCCCCC\">";
					echo "<tr>";
					echo "<td>";
					echo "<table width=\"500\" border=\"0\" cellpadding=\"3\" cellspacing=\"1\" bgcolor=\"#FFFFFF\">";
					echo "<tr>";
					echo "<td width=\"117\">Navn:</td>";
					echo "<td width=\"357\">".$rows2['name']."</td>";
					echo "</tr>";
					echo "<tr>";
					echo "<td valign=\"top\">Kommentar:</td>";
					echo "<td>" . $rows2['commenttext'] . "</td>";
					echo "</tr>";
					echo "<tr>";
					echo "<td valign=\"top\">Tidspunkt:</td>";
					echo "<td>" . $rows2['time'] . "</td>";
					echo "</tr>";
					echo "</table>";
					echo "</td>";
					echo "</tr>";
					echo "</table>";
					echo "<p style=\"clear:both\" />";
				}
		}
		else{
			echo "<h3>Ingen poster her ennå</h3>"; 
			echo  "Alternativet er at noe gikk galt, men det kan jo ikke være tilfelle ;)";
		}	
		
	mysql_close(); //close database
}

function createIndexPagination(){
	$tbl_name="blogpost";
	// Connect to server and select database.
	mysql_connect(DB_HOST, DB_USER, DB_PASSWORD)or die("cannot connect server ");
	mysql_select_db(DB_DATABASE)or die("cannot select DB");
	$category=null;
	if(isset($_GET['cat']))
	{
    	$category = $_GET['cat'];
	}
	// how many rows to show per page
	$rowsPerPage = 10;
	
	// by default we show first page
	$pageNum = 1;
	
	// if $_GET['page'] defined, use it as page number
	if(isset($_GET['page']))
	{
    $pageNum = $_GET['page'];
	}
	// how many rows we have in database
	if($category!=null){
		$query   = "SELECT COUNT(*) AS numrows FROM $tbl_name where category_id=".$category;
	}
	else{
		$query   = "SELECT COUNT(*) AS numrows FROM $tbl_name";
	}
	$result  = mysql_query($query) or die('Error, query failed');
	$row     = mysql_fetch_array($result, MYSQL_ASSOC);
	$numrows = $row['numrows'];
	
	// how many pages we have when using paging?
	$maxPage = ceil($numrows/$rowsPerPage);
	
	// print the link to access each page
	$self = $_SERVER['PHP_SELF'];
	$nav  = '';
	
	/*for($page = 1; $page <= $maxPage; $page++)
	{
	   if ($page == $pageNum)
	   {
	      $nav .= " $page "; // no need to create a link to current page
	   }
	   else
	   {
	      $nav .= " <a href=\"$self?page=$page\" style=\"color:blue\">$page</a> ";
	   }
	} */
	   
	   // creating previous and next link
		// plus the link to go straight to
		// the first and last page
		
		if ($pageNum > 1)
		{
		   $page  = $pageNum - 1;
		   $prev  = " <a href=\"$self?page=$page\" style=\"color:blue\">[Forrige]</a> ";
		
		   $first = " <a href=\"$self?page=1\" style=\"color:blue\">[Første side]</a> ";
		} 
		else
		{
		   $prev  = '&nbsp;'; // we're on page one, don't print previous link
		   $first = '&nbsp;'; // nor the first page link
		}
		
		if ($pageNum < $maxPage)
		{
		   $page = $pageNum + 1;
		   $next = " <a href=\"$self?page=$page\" style=\"color:blue\">[Neste]</a> ";
		
		   $last = " <a href=\"$self?page=$maxPage\" style=\"color:blue\">[Siste side]</a> ";
		} 
		else
		{
		   $next = '&nbsp;'; // we're on the last page, don't print next link
		   $last = '&nbsp;'; // nor the last page link
		}
		
		/*// print the navigation link
		echo $first . $prev . $nav . $next . $last;*/
		// print the navigation link
		echo $first . $prev . 
		" Viser side $pageNum av $maxPage sider " . $next . $last;
		
		
		mysql_close(); //close database
		// ... and we're done!
}
function echoCV(){
	echo '
		<div id="welcome"> 
                  <h2 class="welcome">Curriculum Vitae</h2>
                  <table width="500" border="0" cellpadding="2" cellspacing="1" bgcolor="#E8E8E8">
											<tr>
												<td style="font-weight:bolder; vertical-align:text-top">Utdannelse:</td>
												<td>
													<i>2010-2012:</i> <br/>
													Universitetet i Troms&oslash; - <br/>
													MBA, Strategisk ledelse og &oslash;konomi

													<br/>
													<br/>
												
													<i>2001-2006:</i> <br/>
													Norges teknisk-naturvitenskaplige universitet - <br/>
													Sivilingeni&oslash;r, kommunikasjonsteknologi
													<br/>
													<br/>
													<i>1998-2001:</i> <br/>
													Kval&oslash;ya videreg&aring;ende skole, &Oslash;k.adm
													<br/>
												</td>
												
											</tr>
											<tr>
												<td style="font-weight:bolder; vertical-align:text-top">Jobberfaring:</td>
												<td>
													<i>2006-2009:</i> <br/>
													Accenture - Analyst Programmer<br/>
													<p>Erfaring fra rene utviklingsroller, databaseadministrator,<br/>
													leveranseansvarlig og milj&oslash;administrator. Teknologiene jeg<br/>
													hovedsaklig jobbet med:</p>
													<ul>
														<li>Java SE, Java EE, jhtml</li>
														<li>C# .NET</li>
														<li>MSSQL Server 2005</li>
														<li>Eclipse (+ WSAD og RAD)</li>
														<li>WAS, WPS</li>
														<li>Maven, Ant, CruiseControl, Subversion</li>
													</ul>
													
													<i>2009-2010:</i> <br/>
													Breivoll Inspection Technologies - Systemutvikler<br/>
													<p>Jobbet med utvikling p&aring; verkt&oslash;y for prosessering, analyse<br/>
													og rapportering, systemadministrasjon (linux) og deltatt i <br/>
													produksjon av rapporter til kunder. Design og implementasjon av <br/>
													l&oslash;sninger er gjort med f&oslash;lgende teknologier:</p>
													<ul>
														<li>Java SE (inkl Java Swing)</li>
														<li>OSGi/r-OSGi</li>
														<li>Hadoop/MapReduce</li>
														<li>Eclipse</li>
														<li>Oracle RDMBS</li>
													</ul>
													
													<i>2010-:</i> <br/>
													Facility Management AS - Systemutvikler<br/>
													<p>Videreutvikling og forvaltning av webbasert FDVU-system.<br/>
													   Design og implementasjon av l&oslash;sninger er gjort med <br/>
													  f&oslash;lgende teknologier:</p>
													<ul>
														<li>ASP</li>
														<li>VBScript</li>
														<li>Javascript</li>
														<li>MS Access</li>
													</ul>
												</td>			
											</tr>
											<tr>
												<td style="font-weight:bolder; vertical-align:text-top">Sertifiseringer:</td>
												<td>
													<i>2008:</i> <br/>
													SCJP - Sun Certified Java Programmer
													<br/>
												</td>											
											</tr>
											<tr>
												<td style="font-weight:bolder; vertical-align:text-top">Verv:</td>
												<td>
													<i>2000-2001:</i> <br/>
													Styremedlem, Tromsø Innebandyklubb
													<br/><br/>
													<i>2004-2006:</i> <br/>
													Klassetillitsrepresentant
													<br/><br/>
													<i>2004-2005:</i> <br/>
													Trener for Nidaros A-lag damer
													
												</td>										
											</tr>
										</table>  
                 
                </div>
	
	
	';
}

function displayPictures(){
	echo '
		<div id="welcome"> 
            <h2 class="welcome">Bildeseksjon</h2>
            <p>
              Her kommer mine bilder etterhvert... 
            </p>       
       </div> 
	';
}
function goToGuestBook(){
	header("location: /guestbook/guestbook.php");
}
?>