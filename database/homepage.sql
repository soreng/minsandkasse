CREATE TABLE `guestbook` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 50 ) NOT NULL ,
`email` VARCHAR( 50 ) NOT NULL ,
`message` TEXT NOT NULL ,
`time` TIMESTAMP( 14 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `members` (
  `member_id` int(11) unsigned NOT NULL auto_increment,
  `firstname` varchar(100) default NULL,
  `lastname` varchar(100) default NULL,
  `login` varchar(100) NOT NULL default '',
  `passwd` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`member_id`)
) TYPE=MyISAM;

CREATE TABLE `categories` (
  `category_id` int(11) unsigned NOT NULL auto_increment,
  `name` varchar(20) NOT NULL default '',
  PRIMARY KEY (`category_id`)
) TYPE=MyISAM;

CREATE TABLE `blogpost` (
  `blogpost_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(50) NOT NULL default 'Tittel',	
  `category_id` int(11) unsigned NOT NULL,
  `writtenby_id` int(11) unsigned NOT NULL,
  `blogtext` LONGTEXT NOT NULL default '',
  `time` TIMESTAMP( 14 ) NOT NULL,
  PRIMARY KEY (`blogpost_id`)
) TYPE=MyISAM;

alter table blogpost add foreign key (category_id) references categories(category_id);

alter table blogpost add foreign key (writtenby_id) references members(member_id);

CREATE TABLE `comment` (
  `comment_id` int(11) unsigned NOT NULL auto_increment,
  `blogpost_id` int(11) unsigned NOT NULL,
  `name` VARCHAR( 50 ) NOT NULL ,
  `commenttext` LONGTEXT NOT NULL default '',
  `time` TIMESTAMP( 14 ) NOT NULL,
  PRIMARY KEY (`comment_id`)
) TYPE=MyISAM;

alter table comment add foreign key (blogpost_id) references blogpost(blogpost_id);

CREATE TABLE `album` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 50 ) NOT NULL ,
`cover_image` MEDIUMBLOB,
`timeCreated` TIMESTAMP( 14 ) NOT NULL
) ENGINE = MYISAM ;

CREATE TABLE `picture` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` varchar(25) NOT NULL,
`description` VARCHAR( 50 ) NOT NULL ,
`album_id` INT(10) NOT NULL,
`image` MEDIUMBLOB
) ENGINE = MYISAM ;

alter table picture add foreign key (album_id) references album(id);

CREATE TABLE `thumbnail` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` varchar(25) NOT NULL,
`description` VARCHAR( 50 ) NOT NULL ,
`album_id` INT(10) NOT NULL,
`image` MEDIUMBLOB
) ENGINE = MYISAM ;

alter table thumbnail add foreign key (album_id) references album(id);
