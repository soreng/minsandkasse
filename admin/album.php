<?php	
  require_once('login/auth.php');
  set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/" );
  $action = null;
  
  if(isset($_GET['action'])){
  	$action = $_GET['action']; 
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Sandkassen</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Sandkassen" />
    <meta name="description" content="En plass for å leke seg" />
    <meta name="keywords" content="Stephan, sandbox, php, mysql, ajax, apache2" />
    <meta name="language" content="no" />
    <meta name="subject" content="En plass for å leke seg" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Stephan Kristiansen" />
    <meta name="abstract" content="En plass for å leke seg med programmering og lignende" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <link rel="stylesheet" type="text/css" href="/style.css" />
    <script type="text/javascript" src="/js/jQuery1.4.2.js"></script>
      <script type="text/javascript" src="/js/validateAlbum.js"></script>
  </head>
  <body> 
    <div id="wrapper"> 
      <div id="bg"> 
        <div id="header"></div>  
        <div id="page"> 
          <div id="container"> 
            <!-- banner -->  
            <div id="banner"></div>  
            <!-- end banner -->  
            <!-- horizontal navigation -->  
            <div id="nav1"> 
              <?php 
				include("menu.php"); 
			   ?>
            </div>  
            <!-- end horizontal navigation -->  
            <!--  content -->  
            <div id="content"> 
             
              <div id="center" style="text-align:center"> 
                <br/>
              		Du er logget inn som: <?php echo $_SESSION['SESS_FIRST_NAME'] ." " . $_SESSION['SESS_LAST_NAME'];?>
                
                <?php 
                	if($action!=null){
                		
                		if($action=='new'){
                			echo '
                				
                				<h2>Opprett nytt album</h2>
                				<div id="nyttalbum">
									<form enctype="multipart/form-data" onsubmit="return validate_new_album_form(this)" action="albumHandler.php?action=new" method="post">
										<table width="350" border="0" cellpadding="2" cellspacing="1" bgcolor="#E8E8E8">
												<tr>
													<td>Albumnavn:</td>
													<td style="text-align:left"><input name="albumname" type="text" id="albumname" size="25" />
														<br/><span class="error" name="albumnameError" id="albumnameError"></span>
													</td>
												</tr>
												<tr>
													<td>Albumcover:</td>
													<td style="text-align:left"><input name="albumcover" type="file" id="albumcover" /> 
														<br/><span class="error" name="albumcoverError" id="albumcoverError"></span>
													</td>
												</tr>
												<tr>
													<td>&nbsp;</td>								
													<td><input type="submit" id="submit" name="Submit" value="Lag album"/> </td>
												</tr>
												<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
											</table>
        							</form>
					    
					  				<br/><br/><br/><br/><br/>
					 				Trykk her for å <a href="/login/logout.php" style="color:blue">Logge ut</a>
                				</div>
                			
                			';
                		}
                		
                	}
                
                ?>
                
              </div>  
              <div id="right"> 
                <div id="sidebar"> 
                  <?php 
                  	include("categories.php")
                  ?>  
                  <?php 
                  	include("aboutme.php")
                  ?> 
                  </div> 
                </div> 
              </div>  
              <div class="clear" style="height:40px"/> 
            </div>  
            <!-- end content --> 
          </div>  
          <!-- end container --> 
        </div>  
           <?php 
             include("bottommenu.php")
           ?>
      </div>
      </div>  
  </body>
</html>
