<?php
  set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/" );
  require_once('login/auth.php');

  setlocale(LC_ALL, "no_NO.ISO8859-15");
  $errorDelete=null;
  if(isset($_GET['errorDelete'])){
  	$errorDelete=$_GET['errorDelete'];
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Sandkassen</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Sandkassen" />
    <meta name="description" content="En plass for å leke seg" />
    <meta name="keywords" content="Stephan, sandbox, php, mysql, ajax, apache2" />
    <meta name="language" content="no" />
    <meta name="subject" content="En plass for å leke seg" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Stephan Kristiansen" />
    <meta name="abstract" content="En plass for å leke seg med programmering og lignende" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <link rel="stylesheet" type="text/css" href="/style.css" />
    <script type="text/javascript" src="/js/getblogposts.js"></script>
    <script type="text/javascript" src="/js/jQuery1.4.2.js"></script>
    <script type="text/javascript">
    	window.onLoad = getBlogpostsForDelete();
	</script>   
  </head>
  <body> 
      <div id="wrapper"> 
	      <div id="bg"> 
	        <div id="header"></div>  
	        <div id="page"> 
	          <div id="container"> 
	            <!-- banner -->  
	            <div id="banner"></div>  
	            <!-- end banner -->  
	            <!-- horizontal navigation -->  
	            <div id="nav1"> 
	              <?php 
					include("menu.php"); 
				   ?>
	            </div>  
	            <!-- end horizontal navigation --> 
	            
	              		
	            <!--  content -->  
	            <div id="createpost"> 
	             		<h2>Velg blogpost du vil slette</h2>
	              		<?php 
	              			if($errorDelete!=null){
	              				if($errorDelete=='false'){
	              					echo "<span class=\"success\">Posten ble slettet!</span>";
	              				}
	              				else if($errorDelete=='true'){
	              					echo "<span class=\"error\">Det skjedde noe feil under sletting!</span>";
	              				}
	              			}
	              		?>
	              		<div id="existingblogpost"></div>
	              		
		              		
	            </div>  
	            <br/><br/><br/><br/><br/>
					 Click here to <a href="/login/logout.php" style="color:blue">Log out</a>
	            <!-- end createpost --> 
	          </div>  
	          <!-- end container --> 
	        </div>  
	           <?php 
	             include("bottommenu.php")
	           ?>
	      </div> 
      </div> 
  </body>
</html>
