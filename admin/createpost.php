<?php
  set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/" );
  require_once('login/auth.php');

  setlocale(LC_ALL, "no_NO.ISO8859-15");

  require_once("config.php");
  $id=null;
  if(isset($_GET['blogpostId'])){
  	$id=$_GET['blogpostId'];
  }
  
  $row=null;
  if($id != null){
	$tbl_name="blogpost"; // Table name
	
	// Connect to server and select database.
	mysql_connect(DB_HOST, DB_USER, DB_PASSWORD)or die("cannot connect server ");
	mysql_select_db(DB_DATABASE)or die("cannot select DB");

	$sql="SELECT * FROM $tbl_name where blogpost_id=".$id;
	$result=mysql_query($sql);
	$row = mysql_fetch_object($result);	
	mysql_close(); //close database
  }
  
  function getCategories($categoryId){
  	$tbl_name="categories"; // Table name

	// Connect to server and select database.
	mysql_connect(DB_HOST, DB_USER, DB_PASSWORD)or die("cannot connect server ");
	mysql_select_db(DB_DATABASE)or die("cannot select DB");
	
	$sql="SELECT * FROM $tbl_name ORDER BY category_id ASC";
	$result=mysql_query($sql);
	$resultsize = mysql_num_rows($result);
	
	if($categoryId != ''){
		$value = '';
		while($rows=mysql_fetch_array($result)) {
			if($categoryId == $rows['category_id']){
				$value .= "<option value=" . "\"" . $rows['category_id'] . "\"" . "SELECTED>" . $rows['name'] . "</option>";				
			}
			else{
				$value .= "<option value=" . "\"" . $rows['category_id'] . "\"" . ">" . $rows['name'] . "</option>";
			}				
		 }
	}
	else{
		//add default choice
		$value = "<option value=\"-1\">Velg kategori...</option>";
		while($rows=mysql_fetch_array($result)) {		
				$value .= "<option value=" . "\"" . $rows['category_id'] . "\"" . ">" . $rows['name'] . "</option>";			
		 }
	}
	
	mysql_close(); //close database
	
	return $value;
  }
  
  function editOrCreateNewBlogpost($blogpostId, $categoryId, $row){
	  	if($blogpostId != '' && $categoryId!=''){
	  		echo "<form method=\"post\" id=\"blogpostform\" action=\"\">";
			echo "<table width=\"500\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\" bgcolor=\"#FFFFFF\">";
			echo "<tr><td>Tittel:</td><td><input name=\"title\" type=\"text\" id=\"title\" size=\"25\" value=\"$row->title\"/></td></tr>";
			echo "<tr><td>Kategori:</td><td><select name=\"categories\" id=\"categories\">";
			echo getCategories($categoryId);
			echo "</select></td></tr>";
			echo "</table><br/><br/><br/>";
			echo "<textarea name=\"blogtext\" id=\"blogtext\" cols=\"75\" rows=\"25\" style=\"align:center\">".stripslashes($row->blogtext)."</textarea>";
			echo "<input type=\"submit\" id=\"submit\" name=\"Submit\" value=\"Save\" />";								
			echo "</form>";
			echo "<input type=\"hidden\" id=\"newOrEdit\" name=\"newOrEdit\" value=\"edit\" />";
			echo "<input type=\"hidden\" id=\"blogpostid\" name=\"blogpostid\" value=\"$row->blogpost_id\" />";
	  	}
	  	else{
	  		echo "<form method=\"post\" id=\"blogpostform\" action=\"\">";
			echo "<table width=\"500\" border=\"0\" cellpadding=\"2\" cellspacing=\"1\" bgcolor=\"#FFFFFF\">";
			echo "<tr><td>Tittel:</td><td><input name=\"title\" type=\"text\" id=\"title\" size=\"25\" /></td></tr>";
			echo "<tr><td>Kategori:</td><td><select name=\"categories\" id=\"categories\">";
			echo getCategories('');
			echo "</select></td></tr>";
			echo "</table><br/><br/><br/>";
			echo "<textarea name=\"blogtext\" id=\"blogtext\" cols=\"75\" rows=\"25\" style=\"align:center\">Ny blogpost...</textarea>";
			echo "<input type=\"submit\" id=\"submit\" name=\"Submit\" value=\"Save\" />";								
			echo "</form>";
			echo "<input type=\"hidden\" id=\"newOrEdit\" name=\"newOrEdit\" value=\"new\" />";

	  	}
  }
  	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Sandkassen</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Sandkassen" />
    <meta name="description" content="En plass for å leke seg" />
    <meta name="keywords" content="Stephan, sandbox, php, mysql, ajax, apache2" />
    <meta name="language" content="no" />
    <meta name="subject" content="En plass for å leke seg" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Stephan Kristiansen" />
    <meta name="abstract" content="En plass for å leke seg med programmering og lignende" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <link rel="stylesheet" type="text/css" href="/style.css" />
    <script type="text/javascript" src="/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" src="/js/jQuery1.4.2.js"></script>
    <script type="text/javascript">
	$(document).ready(function() {  
			tinyMCE.init({
				theme : "advanced",
				mode : "textareas"
			});
  			//if submit button is clicked  
  		     $('#submit').click(function () {   
  				 
  		         //Simple validation to make sure user entered something  
  		        $(".error").hide();
				var hasError = false;
		
				var titleVal = $("#title").val();
				if(titleVal == '') {
					$("#title").after('<br><span class="error">Du glemte å skrive inn tittel på blogposten!</span>');
					hasError = true;
				}
		
				var blogpostVal = tinyMCE.get('blogtext').getContent();
				if(blogpostVal == '') {
					$("#blogtext").after('<br><span class="error">Du kan ikke poste en tom blogpost!</span>');
					hasError = true;
				}

				var selectedCategory = $("#categories").val();
				
				if(selectedCategory == -1) {
					$("#categories").after('<br><span class="error">Du må velge en kategori!</span>');
					hasError = true;
				}

				var hiddenVal = $("#newOrEdit").val();
				var hiddenBlogpostId = new String("");
				if(hiddenVal == "edit"){
					hiddenBlogpostId = $("#blogpostid").val();
				}
					
				if(hasError == false) {   
      		         //start the ajax  
      		        
      		         try{
      		        	$.post("addblogpost.php",{ title: titleVal, categoryId: selectedCategory, blogtext: blogpostVal, type: hiddenVal, postId:hiddenBlogpostId },
      		        				  function(data){
	        				  				
      		        						if(data == "success"){
      		        							var successString = "<span class=\"success\">Blogposten ble lagret!</span>";
      		        							document.getElementById("callbackmessage").innerHTML=successString;

      		        						//empty all input
      		        		       		      	$('[name=title]').val('');
      		        		       		        //$('[name=blogtext]').val('Ny blogpost...');
      		        		       		        tinyMCE.get('blogtext').setContent("Ny blogpost...");
      		        		       		        $('[name=categories]').val(-1);
      		        						}
      		        						else if(data == "fail"){
      		        							var successString = "<span class=\"error\">Noe galt skjedde ved lagring av blogposten!</span>";
      		        							document.getElementById("callbackmessage").innerHTML=successString;
          		        					}
      		        						else if(data == "inputerror"){
      		        							var failString = "<span class=\"error\">Det var noe galt med input!</span>";
      		        							document.getElementById("callbackmessage").innerHTML=failString;
      		        						}
      		        						else{
      		        							var failString = "<span class=\"error\">Ukjent feil!</span>";
      		        							document.getElementById("callbackmessage").innerHTML=data;
      		        						}
      		        				  }
      		        				);
      		         }
      		         catch(err){
      		        	txt="There was an error on this page.\n\n";
      		    	  	txt+="Error description: " + err.message + "\n\n";
      		    	  	txt+="Click OK to continue.\n\n";
      		    	  	alert(txt);
	      		     }
     		           		         
				}   

  		         //cancel the submit button default behaviours  
  		         return false;  
  		     }); 
  		});
	</script>
    
    
  </head>
  <body> 
      <div id="wrapper"> 
	      <div id="bg"> 
	        <div id="header"></div>  
	        <div id="page"> 
	          <div id="container"> 
	            <!-- banner -->  
	            <div id="banner"></div>  
	            <!-- end banner -->  
	            <!-- horizontal navigation -->  
	            <div id="nav1"> 
	              <?php 
					include("menu.php"); 
				   ?>
	            </div>  
	            <!-- end horizontal navigation --> 
	            
	              		
	            <!--  content -->  
	            <div id="createpost"> 
	             		<h2>Lag ny blogpost</h2>
	              		
	              		<div id="callbackmessage"></div>
	              		
		              		<div id="editCreateBlogpost">
		              			<?php 
		              				if($row!=null){
		              					try{
		              						editOrCreateNewBlogpost($row->blogpost_id, $row->category_id, $row);
		              					}
		              					  catch(Exception $e){
	  										echo 'Caught exception: ',  $e->getMessage(), "\n";
	  									}
		              				}
		              				else{
		              					try{
		              						editOrCreateNewBlogpost('', '', null);
		              					}
		              					  catch(Exception $e){
	  										echo 'Caught exception: ',  $e->getMessage(), "\n";
	  									}		              				}
		              			?>
		              		</div>
             
	            </div>  
	            <br/><br/><br/><br/><br/>
					 Click here to <a href="/login/logout.php" style="color:blue">Log out</a>
	            <!-- end createpost --> 
	          </div>  
	          <!-- end container --> 
	        </div>  
	           <?php 
	             include("bottommenu.php")
	           ?>
	      </div> 
      </div> 
  </body>
</html>
