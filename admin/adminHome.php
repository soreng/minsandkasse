<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/" );	
  require_once('login/auth.php');
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Sandkassen</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Sandkassen" />
    <meta name="description" content="En plass for å leke seg" />
    <meta name="keywords" content="Stephan, sandbox, php, mysql, ajax, apache2" />
    <meta name="language" content="no" />
    <meta name="subject" content="En plass for å leke seg" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Stephan Kristiansen" />
    <meta name="abstract" content="En plass for å leke seg med programmering og lignende" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <link rel="stylesheet" type="text/css" href="/style.css" />
  </head>
  <body> 
    <div id="wrapper"> 
      <div id="bg"> 
        <div id="header"></div>  
        <div id="page"> 
          <div id="container"> 
            <!-- banner -->  
            <div id="banner"></div>  
            <!-- end banner -->  
            <!-- horizontal navigation -->  
            <div id="nav1"> 
              <?php 
				include("menu.php"); 
			   ?>
            </div>  
            <!-- end horizontal navigation -->  
            <!--  content -->  
            <div id="content"> 
             
              <div id="center" style="text-align:center"> 
                <br/>
              		Du er logget inn som: <?php echo $_SESSION['SESS_FIRST_NAME'] ." " . $_SESSION['SESS_LAST_NAME'];?>
                <h2>Administrer blogposter</h2>
                <div id="adminMenu">
					<table align="center">
					    <tr>
					      <td width="125"><a href="/admin/createpost.php"><img alt="Create post"
					        src="/images/document_64.png" width="64"
					        height="64" /></a></td>
					      <td width="125"><a href="/admin/selecteditblogpost.php"><img alt="Edit post"
					        src="/images/document_pencil_64.png" width="64"
					        height="64" /></a></td>
					      <td width="125"><a href="/admin/selectdeleteblogpost.php"><img alt="Delete post"
					        src="/images/delete-icon.png" width="64"
					        height="64" /></a></td>
					    </tr>
					    <tr>
					      <td width="125">Create post</td>
					      <td width="125">Edit posts</td>
					      <td width="125">Delete posts</td>
					    </tr>
					    <tr style="height:20px">
					      <td width="125"></td>
					      <td width="125"></td>
					      <td width="125"></td>
					    </tr>
					  </table>
					 <!--  <h2>Administrer gjesteboken</h2>
					  <table align="center"> 
					    <tr>
					      <td width="125"><a href="/admin/editguestbook.php"><img alt="Edit guestbook"
					        src="/images/document_pencil_64.png" width="64"
					        height="64" /></a></td>
					      <td width="150"><a href="/admin/deleteguestbookposts.php"><img alt="Delete posts"
					        src="/images/delete-icon.png" width="64"
					        height="64" /></a></td>
					      
					    </tr>
					    <tr>
					      <td width="125">Edit guestbook</td>
					      <td width="125">Delete posts</td>
					     
					    </tr>
					  </table> --> 
					  <br/><br/><br/><br/><br/>
					 Click here to <a href="/login/logout.php" style="color:blue">Log out</a>
                </div>
              </div>  
              <div id="right"> 
                <div id="sidebar"> 
                  <?php 
                  	include("categories.php")
                  ?>  
                  <?php 
                  	include("aboutme.php")
                  ?> 
                  </div> 
                </div> 
              </div>  
              <div class="clear" style="height:40px"/> 
            </div>  
            <!-- end content --> 
          </div>  
          <!-- end container --> 
        </div>  
           <?php 
             include("bottommenu.php")
           ?>
      </div>  
  </body>
</html>
