<?php
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/" );
require_once("config.php");

$getComments = null;

if(isset($_GET['id'])){
	$getComments=$_GET['id'];
}

if($getComments!=null && $getComments== 'getcomments'){
	getComments();
}

function getComments(){
		
	$tbl_name="guestbook"; // Table name
	
	// Connect to server and select database.
	mysql_connect(DB_HOST, DB_USER, DB_PASSWORD)or die("cannot connect server ");
	mysql_select_db(DB_DATABASE)or die("cannot select DB");
	
	// how many rows to show per page
	$rowsPerPage = 10;
		
	// by default we show first page
	$pageNum = 1;
	// if $_GET['page'] defined, use it as page number
	if(isset($_GET['page']))
	{
	   $pageNum = $_GET['page'];
	}
		
	// counting the offset
	$offset = ($pageNum - 1) * $rowsPerPage;
	
	$sql="SELECT * FROM $tbl_name ORDER BY id DESC LIMIT $offset, $rowsPerPage";
	$result=mysql_query($sql);
	$resultsize = mysql_num_rows($result);
	if($resultsize == 0){
	
		echo "<p style=\"clear:both\" />";
		echo "<table width=\"250\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"1\" bgcolor=\"#CCCCCC\">";
		echo "<tr>";
		echo "<td>Bli den første til å legge til en kommentar!</td>";
		echo "</tr>";
		echo "</table>"; 
	}
	while($rows=mysql_fetch_array($result)) {
			$timezone = new DateTimeZone( "Europe/Oslo" ); 
			$dateRaw = new DateTime($rows['time']); 
			$dateRaw->setTimezone( $timezone );
			$dateAsString = $dateRaw->format("Y-m-d H:i:s"); 
	 		//$date= substr($rows['time'],8,2).".".substr($rows['time'],5,2).".".substr($rows['time'],2 ,2)." kl. ";
	 		$date= substr($dateAsString,8,2).".".substr($dateAsString,5,2).".".substr($dateAsString,2 ,2);
			echo "<table width=\"500\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"1\" bgcolor=\"#CCCCCC\">";
			echo "<tr>";
			echo "<td>";
			echo "<table width=\"500\" border=\"0\" cellpadding=\"3\" cellspacing=\"1\" bgcolor=\"#FFFFFF\">";
			echo "<tr>";
			echo "<td width=\"117\">Navn:</td>";
			echo "<td width=\"357\">" . $rows['name'] . "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td>E-post:</td>";
			echo "<td>" . $rows['email'] . "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td valign=\"top\">Kommentar:</td>";
			echo "<td>" . $rows['message'] . "</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td valign=\"top\">Dato:</td>";
			echo "<td>" . $date . "</td>";
			echo "</tr>";
			echo "</table>";
			echo "</td>";
			echo "</tr>";
			echo "</table>";		
			echo "<p style=\"clear:both\" />";
	 }
	mysql_close(); //close database
}

function createPagination(){
	$tbl_name="guestbook";
	// Connect to server and select database.
	mysql_connect(DB_HOST, DB_USER, DB_PASSWORD)or die("cannot connect server ");
	mysql_select_db(DB_DATABASE)or die("cannot select DB");
	// how many rows to show per page
	$rowsPerPage = 10;
	
	// by default we show first page
	$pageNum = 1;
	
	// if $_GET['page'] defined, use it as page number
	if(isset($_GET['page']))
	{
    $pageNum = $_GET['page'];
	}
	// how many rows we have in database
	$query   = "SELECT COUNT(*) AS numrows FROM $tbl_name";
	$result  = mysql_query($query) or die('Error, query failed');
	$row     = mysql_fetch_array($result, MYSQL_ASSOC);
	$numrows = $row['numrows'];
	
	// how many pages we have when using paging?
	$maxPage = ceil($numrows/$rowsPerPage);
	
	// print the link to access each page
	$self = $_SERVER['PHP_SELF'];
	$nav  = '';
	   
	   // creating previous and next link
		// plus the link to go straight to
		// the first and last page
		
		if ($pageNum > 1)
		{
		   $page  = $pageNum - 1;
		   $prev  = " <a href=\"$self?site=guestbook&page=$page\" style=\"color:blue\">[Forrige]</a> ";
		
		   $first = " <a href=\"$self?site=guestbook&page=1\" style=\"color:blue\">[Første side]</a> ";
		} 
		else
		{
		   $prev  = '&nbsp;'; // we're on page one, don't print previous link
		   $first = '&nbsp;'; // nor the first page link
		}
		
		if ($pageNum < $maxPage)
		{
		   $page = $pageNum + 1;
		   $next = " <a href=\"$self?site=guestbook&page=$page\" style=\"color:blue\">[Neste]</a> ";
		
		   $last = " <a href=\"$self?site=guestbook&page=$maxPage\" style=\"color:blue\">[Siste side]</a> ";
		} 
		else
		{
		   $next = '&nbsp;'; // we're on the last page, don't print next link
		   $last = '&nbsp;'; // nor the last page link
		}
		
		/*// print the navigation link
		echo $first . $prev . $nav . $next . $last;*/
		// print the navigation link
		echo $first . $prev . 
		" Viser side $pageNum av $maxPage sider " . $next . $last;
				
		mysql_close(); //close database
		// ... and we're done!
}
?>

