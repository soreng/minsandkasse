<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/" ); 
require_once("viewguestbook.php");
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Sandkassen</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Sandkassen" />
    <meta name="description" content="En plass for å leke seg" />
    <meta name="keywords" content="Stephan, sandbox, php, mysql, ajax, apache2" />
    <meta name="language" content="no" />
    <meta name="subject" content="En plass for å leke seg" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Stephan Kristiansen" />
    <meta name="abstract" content="En plass for å leke seg med programmering og lignende" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <link rel="stylesheet" type="text/css" href="../style.css" />
     <script type="text/javascript" src="/js/jQuery1.4.2.js"></script>
      <!--<script type="text/javascript" src="/js/getcomments.js"></script>-->
      <script type="text/javascript" src="/js/addGuestbookPost.js"></script>
      <script type="text/javascript">
      		//var pageNum = getQueryParameter('page');                                         
      		//window.onLoad = getComments(pageNum);
      		// $(document).ready(function() {  
      			//if submit button is clicked  
      		   //  $('#submit').click(function () {   
      				alert("test"); 
      		    //     addComment();
					//validateInput();
      		         //cancel the submit button default behaviours  
      		      //   return false;  
      		     //}); 
      		//});     	   			                                     
 	</script>
  </head>
  <body> 
    <div id="wrapper"> 
      <div id="bg"> 
        <div id="header"></div>  
        <div id="page"> 
          <div id="container"> 
            <!-- banner -->  
            <div id="banner"></div>  
            <!-- end banner -->  
            <!-- horizontal navigation -->  
            <div id="nav1"> 
              <?php 
				include("menu.php"); 
			   ?>
            </div>  
            <!-- end horizontal navigation -->  
            <!--  content -->  
            <div id="content"> 
              <div id="center">
              <?php 
				// by default we show first page
				$pageNum = null;
				// if $_GET['page'] defined, use it as page number
				if(isset($_GET['page']))
				{
				   $pageNum = $_GET['page'];
				} 
				
				if($pageNum==null || $pageNum==1){
					echo ' <table width="500" border="0" align="center" cellpadding="0" cellspacing="1">
						<tr>
							<td><h3>Legg igjen en kommentar i gjesteboka! </h3></td>
						</tr>
					  </table>
		              <p style="clear:both" />
		              
						  <table width="500" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#D8D8D8">
								<tr>
									<td>
										<form id="form1" name="form1" onsubmit="return validate_form(this)" method="post" action="addguestbook.php">
										
										<table width="500" border="0" cellpadding="2" cellspacing="1" bgcolor="#E8E8E8">
											<tr>
												<td>Navn:</td>
												<td><input name="name" type="text" id="name" size="25" />
													<br/><span class="error" name="nameError" id="nameError"></span>
												</td>
											</tr>
											<tr>
												<td>E-post:</td>
												<td><input name="email" type="text" id="email" size="25" />
													<br/><span class="error" name="emailError" id="emailError"></span>
												</td>
											</tr>
											<tr>
												<td valign="top">Kommentar:</td>
												<td><textarea name="comment" cols="40" rows="3" id="comment"></textarea>
													<br/><span class="error" name="commentError" id="commentError"></span>
												</td>
											</tr>
											<tr>
												<td>Ikke søppel*:</td>
												<td><input type=checkbox name="notspamconfirm" id="notspamconfirm"/> 
													<br/><span class="error" name="notspamError" id="notspamError"></span>
												</td>
											</tr>
											<tr>
												<td>&nbsp;</td>								
												<td><input type="submit" id="submit" name="Submit" value="Legg til" onClick="javascript:addComment();"/> </td>
											</tr>
											<tr>
												<td>&nbsp;</td>
												<td>* indikerer påkrevd felt</td>
											</tr>
										</table>
										</form>
									</td>						
								</tr>
							</table>
						<p style="clear:both" />
						<p style="clear:both" />				
					';
				}
			   ?>  
             
					<div id="guestbookposts">
						<?php getComments();?>
					</div>
					<div id="pagination" style="text-align:center"><?php createPagination();?></div>
              </div>  
	              <div id="right"> 
	                <div id="sidebar"> 
	                  <?php 
	                  	include("categories.php")
	                  ?>  
	                  <?php 
	                  	include("aboutme.php");
	                  	include("rightside.php");
	                  ?> 
	                 </div> 
	              </div> 
              </div>  
              <div class="clear" style="height:40px"/> 
            </div>  
            <!-- end content --> 
          </div>  
          <!-- end container --> 
           
        </div>  
           <?php 
             include("bottommenu.php")
           ?>
      </div>  
  </body>
</html>