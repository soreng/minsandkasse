<?php
  set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/" );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Sandkassen</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Sandkassen" />
    <meta name="description" content="En plass for å leke seg" />
    <meta name="keywords" content="Stephan, sandbox, php, mysql, ajax, apache2" />
    <meta name="language" content="no" />
    <meta name="subject" content="En plass for å leke seg" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Stephan Kristiansen" />
    <meta name="abstract" content="En plass for å leke seg med programmering og lignende" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <link rel="stylesheet" type="text/css" href="/style.css" />
  </head>
  <body> 
    <div id="wrapper"> 
      <div id="bg"> 
        <div id="header"></div>  
        <div id="page"> 
          <div id="container"> 
            <!-- banner -->  
            <div id="banner"></div>  
            <!-- end banner -->  
            <!-- horizontal navigation -->  
            <div id="nav1"> 
              <?php 
				include("menu.php"); 
			   ?>
            </div>  
            <!-- end horizontal navigation -->  
            <!--  content -->  
            <div id="content"> 
              <div id="center"> 
                
	                <h2>Du må være logget inn for å kunne administrere siden</h2>
	                <div id="adminMenu">
						<form id="loginForm" name="loginForm" method="post" action="/login/login-exec.php">
							  <table width="300" border="0" align="center" cellpadding="2" cellspacing="0">
							    <tr>
							      <td width="50"><b>Username</b></td>
							      <td width="188"><input name="login" type="text" class="textfield" id="login" /></td>
							    </tr>
							    <tr>
							      <td><b>Password</b></td>
							      <td><input name="password" type="password" class="textfield" id="password" /></td>
							    </tr>
							    <tr>
							      <td>&nbsp;</td>
							      <td><input type="submit" name="Submit" value="Login" /></td>
							    </tr>
							  </table>
							</form>
	                </div>
	              </div>  
	              <div id="right"> 
	                <div id="sidebar"> 
	                  <?php 
	                  	include("categories.php");
	                  ?>  
	                  <?php 
	                  	include("aboutme.php");
	                  	include("rightside.php");
	                  ?> 
	                  </div> 
	                </div> 
	           </div> 
              </div>  
              <div class="clear" style="height:40px"/> 
            </div>  
            <!-- end content --> 
          </div>  
          <!-- end container --> 
        </div>  
           <?php 
             include("bottommenu.php")
           ?>
      </div>  
  </body>
</html>
