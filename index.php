<?php
session_start();
ob_start();
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'; 
set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/" );
require_once("indexhelper.php");
require_once("guestbook/viewguestbook.php");
$site=null;
$blogid=null;
if(isset($_GET['blogid'])){
	$blogid=$_GET['blogid'];
}
if(isset($_GET['site'])){
	$site=$_GET['site'];
}
$captchaError=null;
if(isset($_GET['captchaError'])){
	$captchaError=$_GET['captchaError'];
}

$nameError=null;
if(isset($_GET['nameError'])){
	$nameError=$_GET['nameError'];
}

$messageError=null;
if(isset($_GET['messageError'])){
	$messageError=$_GET['messageError'];
}
     
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Sandkassen</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Sandkassen" />
    <meta name="description" content="En plass for å leke seg" />
    <meta name="keywords" content="Stephan, sandbox, php, mysql, ajax, apache2" />
    <meta name="language" content="no" />
    <meta name="subject" content="En plass for å leke seg" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Stephan Kristiansen" />
    <meta name="abstract" content="En plass for å leke seg med programmering og lignende" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <meta name="google-site-verification" content="zmFtgrIZh9lwSfpAc1ULVizJMsdM5H7DflfgEiCe8HU" />
    <link rel="stylesheet" type="text/css" href="style.css" />
    <script type="text/javascript" src="/js/addGuestbookPost.js"></script>
    <script type="text/javascript" src="/js/addcomment.js"></script>
    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-19097057-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
       })();
     </script>
  </head>
  <body> 
    <div id="wrapper"> 
      <div id="bg"> 
        <div id="header"></div>  
        <div id="page"> 
          <div id="container"> 
            <!-- banner -->  
            <div id="banner"></div>  
            <!-- end banner -->  
            <!-- horizontal navigation -->  
            <div id="nav1"> 
              <?php 
				include("menu.php"); 
			   ?>
            </div>  
            <!-- end horizontal navigation -->  
            <!--  content -->  
            <div id="content"> 
              <div id="center">
              <?php 
               if($site==null || $site=='start'){
	              echo "  
               	    <div id=\"welcome\"> 
	                  <h2 class=\"welcome\">Velkommen</h2>
	                  <p>
						Takk for at du titter innom! Dette blir å fungere som min hjemmeside/blog. Håper jeg blir å kunne være litt mer
						aktiv på å poste nyheter når jeg har laget hjemmesiden selv. Dessverre er jeg ingen 
						designer, så siden er neppe eyecandy for de som kan sånt.
	                  </p>  
	                 
	                </div>
	                <div id=\"blogposts\">";
	                
	                	getBlogposts();
	               echo " 
	                </div>
	                <br/><br/>
	                 <div id=\"pagination\" style=\"text-align:center\">"; 
	               		createIndexPagination(); 
	               echo "</div>";
	             }
	              if($site=='cv'){
	             	echoCV();
	             } 
	             if($site=='pictures'){
	             	displayPictures();
	             }
	             if($site=='displayPost'){
	             	if($blogid!=null || $blogid==''){

	             		displayPost($blogid,$captchaError,$nameError,$messageError);
	             	}
	             	else{
	             		header("location: /?site=start");
	             	}
	             }
	             if($site=='guestbook'){ 
					// by default we show first page
					$pageNum = null;
					// if $_GET['page'] defined, use it as page number
					if(isset($_GET['page']))
					{
					   $pageNum = $_GET['page'];
					} 
					
					if($pageNum==null || $pageNum==1){
						echo ' <table width="500" border="0" align="center" cellpadding="0" cellspacing="1">
							<tr>
								<td><h3>Legg igjen en kommentar i gjesteboka! </h3></td>
							</tr>
						  </table>
			              <p style="clear:both" />
			              
							  <table width="500" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#D8D8D8">
									<tr>
										<td>
											<form id="form1" name="form1" onsubmit="return validate_form(this)" method="post" action="/guestbook/addguestbook.php">
											
											<table width="500" border="0" cellpadding="2" cellspacing="1" bgcolor="#E8E8E8">
												<tr>
													<td>Navn:</td>
													<td><input name="name" type="text" id="name" size="25" />
														<br/><span class="error" name="nameError" id="nameError"></span>
													</td>
												</tr>
												<tr>
													<td>E-post:</td>
													<td><input name="email" type="text" id="email" size="25" />
														<br/><span class="error" name="emailError" id="emailError"></span>
													</td>
												</tr>
												<tr>
													<td valign="top">Kommentar:</td>
													<td><textarea name="comment" cols="40" rows="3" id="comment"></textarea>
														<br/><span class="error" name="commentError" id="commentError"></span>
													</td>
												</tr>
												<tr>
													<td>Skriv inn tekst på bildet:</td>
													<td><input type="text" name="captcha_code" size="10" maxlength="6" />';
													if($captchaError!=null && $captchaError=='true'){
														echo '<br/><span class="error" name="captchaError" id="captchaError">
																Du skrev ikke inn rett tekst, gjør et nytt forsøk!
															</span>';
													}
												echo	'</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td><img id="captcha" src="/captcha/securimage_show.php" alt="CAPTCHA Image" />
													
													</td>
												</tr>
												<tr>
													<td>&nbsp;</td>								
													<td><input type="submit" id="submit" name="Submit" value="Legg til"/> </td>
												</tr>
												
											</table>
											</form>
										</td>						
									</tr>
								</table>
							<p style="clear:both" />
							<p style="clear:both" />				
						';
					}
				   ?>  
	             
						<div id="guestbookposts">
							<?php getComments();?>
						</div>
						<div id="pagination" style="text-align:center"><?php createPagination();?></div>
		            <?php 
	             		}
		            ?> 	               
              </div>  
              <div id="right"> 
                <div id="sidebar"> 
                  <?php 
                  	include("categories.php");
                  	include("aboutme.php");
                  	include("rightside.php");
                  ?> 
                  </div> 
                </div> 
              </div>  
              <div class="clear" style="height:40px"/> 
            </div>  
            <!-- end content --> 
          </div>  
          <!-- end container --> 
          
        </div>  
           <?php 
             include("bottommenu.php")
           ?>
      </div>  
  </body>
</html>
<?php 
ob_flush();
?>