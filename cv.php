<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php set_include_path( get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . "/" );
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Sandkassen</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Sandkassen" />
    <meta name="description" content="En plass for å leke seg" />
    <meta name="keywords" content="Stephan, sandbox, php, mysql, ajax, apache2" />
    <meta name="language" content="no" />
    <meta name="subject" content="En plass for å leke seg" />
    <meta name="robots" content="All" />
    <meta name="copyright" content="Stephan Kristiansen" />
    <meta name="abstract" content="En plass for å leke seg med programmering og lignende" />
    <meta name="MSSmartTagsPreventParsing" content="true" />
    <link rel="stylesheet" type="text/css" href="style.css" />
  </head>
  <body> 
    <div id="wrapper"> 
      <div id="bg"> 
        <div id="header"></div>  
        <div id="page"> 
          <div id="container"> 
            <!-- banner -->  
            <div id="banner"></div>  
            <!-- end banner -->  
            <!-- horizontal navigation -->  
            <div id="nav1"> 
              <?php 
				include("menu.php"); 
			   ?>
            </div>  
            <!-- end horizontal navigation -->  
            <!--  content -->  
            <div id="content"> 
              <div id="center"> 
                <div id="welcome"> 
                  <h2 class="welcome">Curriculum Vitae</h2>
                  <table width="500" border="0" cellpadding="2" cellspacing="1" bgcolor="#E8E8E8">
											<tr>
												<td style="font-weight:bolder; vertical-align:text-top">Utdannelse:</td>
												<td>
													<i>2010-2012:</i> <br/>
													Universitetet i Troms&oslash; - <br/>
													MBA, Strategisk ledelse og &oslash;konomi

													<br/>
												
													<i>2001-2006:</i> <br/>
													Norges teknisk-naturvitenskaplige universitet - <br/>
													Sivilingeni&oslash;r, kommunikasjonsteknologi

													<br/>
													<i>1998-2001:</i> <br/>
													Kval&oslash;ya videreg&aring;ende skole, &Oslash;k.adm
													<br/>
												</td>
												
											</tr>
											<tr>
												<td style="font-weight:bolder; vertical-align:text-top">Jobberfaring:</td>
												<td>
													<i>2006-2009:</i> <br/>
													Accenture - Analyst Programmer<br/>
													<p>Erfaring fra rene utviklingsroller, databaseadministrator,<br/>
													leveranseansvarlig og milj&oslash;administrator. Teknologiene jeg<br/>
													hovedsaklig jobbet med:</p>
													<ul>
														<li>Java SE, Java EE, jhtml</li>
														<li>C# .NET</li>
														<li>MSSQL Server 2005</li>
														<li>Eclipse (+ WSAD og RAD)</li>
														<li>WAS, WPS</li>
														<li>Maven, Ant, CruiseControl, Subversion</li>
													</ul>
													
													<i>2009-2010:</i> <br/>
													Breivoll Inspection Technologies - Systemutvikler<br/>
													<p>Jobbet med utvikling p&aring; verkt&oslash;y for prosessering, analyse<br/>
													og rapportering, systemadministrasjon (linux) og deltatt i <br/>
													produksjon av rapporter til kunder. Design og implementasjon av <br/>
													l&oslash;sninger er gjort med f&oslash;lgende teknologier:</p>
													<ul>
														<li>Java SE (inkl Java Swing)</li>
														<li>OSGi/r-OSGi</li>
														<li>Hadoop/MapReduce</li>
														<li>Eclipse</li>
														<li>Oracle RDMBS</li>
													</ul>
													
													<i>2010-:</i> <br/>
													Facility Management AS - Systemutvikler<br/>
													<p>Videreutvikling og forvaltning av webbasert FDVU-system.<br/>
													   Design og implementasjon av <br/>
													l&oslash;sninger er gjort med f&oslash;lgende teknologier:</p>
													<ul>
														<li>ASP</li>
														<li>VBScript</li>
														<li>Javascript</li>
														<li>MS Access</li>
													</ul>
												</td>			
											</tr>
											<tr>
												<td style="font-weight:bolder; vertical-align:text-top">Sertifiseringer:</td>
												<td>
													<i>2008:</i> <br/>
													SCJP - Sun Certified Java Programmer
													<br/>
												</td>											
											</tr>
											<tr>
												<td style="font-weight:bolder; vertical-align:text-top">Verv:</td>
												<td>
													<i>2000-2001:</i> <br/>
													Styremedlem, Tromsø Innebandyklubb
													<br/><br/>
													<i>2004-2006:</i> <br/>
													Klassetillitsrepresentant
													<br/><br/>
													<i>2004-2005:</i> <br/>
													Trener for Nidaros A-lag damer
													
												</td>										
											</tr>
										</table>  
                 
                </div> 
              </div>  
              <div id="right"> 
                <div id="sidebar"> 
                  <?php 
                  	include("categories.php");
                  	include("aboutme.php");
                  	include("rightside.php");
                  ?> 
                  </div> 
                </div> 
              </div>  
              <div class="clear" style="height:40px"/> 
            </div>  
            <!-- end content --> 
          </div>  
          <!-- end container --> 
        </div>  
           <?php 
             include("bottommenu.php")
           ?>
      </div>
      </div>  
  </body>
</html>