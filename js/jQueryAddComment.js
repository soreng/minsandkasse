$(document).ready(function() {  
      			//if submit button is clicked  
      		     $('#submit').click(function () {   
      				 
      		         //Simple validation to make sure user entered something  
      		        $(".error").hide();
					var hasError = false;
					var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			
					var emailVal = $("#email").val();
					if(emailVal == '') {
						$("#email").after('<br><span class="error">Du glemte å skrive inn e-post adressen din.</span>');
						hasError = true;
					} else if(!emailReg.test(emailVal)) {
						$("#email").after('<br><span class="error">Du må skrive inn en gyldig e-post adresse.</span>');
						hasError = true;
					}
			
					var nameVal = $("#name").val();
					if(nameVal == '') {
						$("#name").after('<br><span class="error">Du glemte å skrive inn navnet ditt.</span>');
						hasError = true;
					}
			
					var commentVal = $("#comment").val();
					if(commentVal == '') {
						$("#comment").after('<br><span class="error">Du glemte å skrive en beskjed.</span>');
						hasError = true;
					}
 
					if(hasError == false) {
						//organize the data properly  
	      		         var data = 'name=' + nameVal + '&email=' + emailVal+ '&message='  + encodeURIComponent(commentVal);  
	      		           
	      		         //disabled all the text fields  
	      		         //$('.text').attr('disabled','true');  
	      		           
	      		         //show the loading sign  
	      		         $('.loading').show();  
	      		           
	      		         //start the ajax  
	      		         
	      		         $.ajax({  
	      		             //this is the php file that processes the data and send mail  
	      		             url: "addguestbook.php",   
	      		               
	      		             //POST method is used  
	      		             type: "POST",  
	      		   
	      		             //pass the data           
	      		             data: data,       
	      		               
	      		             //Do not cache the page  
	      		             cache: false,  
	      		               
	      		             //success  
	      		             success: function (html) {                
	      		                  
	      		                 if (html==1) {                    
	      		                     //hide the form  
	      		                     //$('.form').fadeOut('slow');                   
	      		                       
	      		                     //show the success message  
	      		                     $('.done').fadeIn('slow');  
	      		                       
	      		                  
	      		                 } else alert('Sorry, unexpected error. Please try again later.');                 
	      		             }         
	      		         });
	      		         
					}   
      		           
      		           
      		         //cancel the submit button default behaviours  
      		         return false;  
      		     }); 
      		});     	