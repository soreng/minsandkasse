/* document.write("<script type='text/javascript' src='/homepage/js/jQuery1.4.2.js'></scr"+"ipt>");
*/
function addComment()
{
	//Simple validation to make sure user entered something  
      $(".error").hide();
	var hasError = false;
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

	var emailVal = $("#email").val();
	if(emailVal == '') {
		$("#email").after('<br><span class="error">Du glemte å skrive inn e-post adressen din.</span>');
		hasError = true;
	} else if(!emailReg.test(emailVal)) {
		$("#email").after('<br><span class="error">Du må skrive inn en gyldig e-post adresse.</span>');
		hasError = true;
	}

	var nameVal = $("#name").val();
	if(nameVal == '') {
		$("#name").after('<br><span class="error">Du glemte å skrive inn navnet ditt.</span>');
		hasError = true;
	}
	/*if (!isChecked) {
		$("#notspamconfirm").after('<br><span class="error">Er dette søppelpost?</span>');
		hasError = true;
	}*/

	var commentVal = $("#comment").val();
	if(commentVal == '') {
		$("#comment").after('<br><span class="error">Du glemte å skrive en beskjed.</span>');
		hasError = true;
	}
	
	//var isChecked = $('input[name=notspamconfirm]').is(':checked');
	
	
	if(hasError == false) {   
	         //start the ajax 
			
	         try{
	        	$.post("addguestbook.php",{ name: nameVal, email: emailVal, comment: commentVal },
	        				  function(data){
	        						document.getElementById("guestbookposts").innerHTML=data;
	        				  }
	        				);
	         }
	         catch(err){
	        	txt="There was an error on this page.\n\n";
	    	  	txt+="Error description: " + err.message + "\n\n";
	    	  	txt+="Click OK to continue.\n\n";
	    	  	alert(txt);
		     }

	     //empty all input
	      	$('[name=name]').val('');
	        $('[name=email]').val('');
	        $('[name=comment]').val('');
	        //$('[name=notspamconfirm]').checked(false);      		         
	}
}
function validate_form(thisForm){
	var name = thisForm.name.value;
	var email = thisForm.email.value;
	var comment = thisForm.comment.value;
	//var isChecked = thisForm.notspamconfirm.checked;
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var returnTrue=true;
	try{
		
		if(email==""){
			returnTrue=false;
			document.getElementById("emailError").innerHTML="Du glemte å skrive inn e-post adressen din.";
			thisForm.email.focus();
		}
		else if(!emailReg.test(email)) {
			document.getElementById("emailError").innerHTML="Du må skrive inn en gyldig e-post adresse.";
			thisForm.email.focus();
			returnTrue=false;
		}
		else{
			document.getElementById("emailError").innerHTML="";
		}
		if(name==""){
			returnTrue=false;
			document.getElementById("nameError").innerHTML="Du glemte å skrive inn navnet ditt.";
			thisForm.name.focus();
		}
		else{
			document.getElementById("nameError").innerHTML="";	
		}
		if(comment==""){
			returnTrue=false;
			document.getElementById("commentError").innerHTML="Du glemte å skrive en beskjed.";
			thisForm.comment.focus();
		}
		else{
			document.getElementById("commentError").innerHTML="";	
		}

		/*if (!isChecked) {
			document.getElementById("notspamError").innerHTML="Er dette søppelpost?";
			returnTrue = false;
		}
		else{
			document.getElementById("notspamError").innerHTML="";
		}*/
	}
	 catch(err){
     	txt="There was an error on this page.\n\n";
 	  	txt+="Error description: " + err.message + "\n\n";
 	  	txt+="Click OK to continue.\n\n";
 	  	alert(txt);
	     }
	return returnTrue;
}