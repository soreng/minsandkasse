function validate_new_album_form(thisForm){
	var albumname = thisForm.albumname.value;
	var file = thisForm.albumcover.value;

	
	var returnTrue=true;
	try{
		if(albumname==""){
			returnTrue=false;
			document.getElementById("albumnameError").innerHTML="Du glemte å skrive inn albumets navn.";
			thisForm.albumname.focus();
		}
		else{
			document.getElementById("albumnameError").innerHTML="";	
		}
		if (file=="") {
			document.getElementById("albumcoverError").innerHTML="Du må laste opp et albumcover.";
			returnTrue = false;
		}
		else{
			var ext = file.substring(file.lastIndexOf('.') + 1);
			if(ext == "gif" || ext == "GIF" || ext == "JPEG" || ext == "jpeg" || ext == "jpg" || ext == "JPG" || ext == "png" || ext == "PNG")
			{
				document.getElementById("albumcoverError").innerHTML="";
			} 
			else
			{
				returnTrue=false;
				document.getElementById("albumcoverError").innerHTML="Tillatte filtyper er: jpg, png og gif ";
				thisForm.albumcover.focus();
			}
		}
	}
	 catch(err){
     	txt="There was an error on this page.\n\n";
 	  	txt+="Error description: " + err.message + "\n\n";
 	  	txt+="Click OK to continue.\n\n";
 	  	alert(txt);
 	  	return false;
	     }
	return returnTrue;
}